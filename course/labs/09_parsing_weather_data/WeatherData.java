import edu.duke.*;
import org.apache.commons.csv.*;
import java.io.*;

public class WeatherData {
   public CSVRecord hottestHourInFile(CSVParser parser) {
    //start with largestSoFar as nothing
    CSVRecord largestSoFar = null;
    //For each row (currentRow) in the CSV File
    for (CSVRecord currentRow : parser) {
    // use method to compare two records
        largestSoFar = getLargestOfTwo(currentRow, largestSoFar);
    }
    //The largestSoFar is the answer
    return largestSoFar;
   }

   public CSVRecord hottestInManyDays() {
       CSVRecord largestSoFar = null;
       DirectoryResource dr = new DirectoryResource();
       // iterate over files
       for (File f : dr.selectedFiles()) {
           FileResource fr = new FileResource(f);
           // use method to get largest in file.
           CSVRecord currentRow = hottestHourInFile(fr.getCSVParser());
           // use method to compare two records
           largestSoFar = getLargestOfTwo(currentRow, largestSoFar);
       }
        //The largestSoFar is the answer
       return largestSoFar;
    }

    public CSVRecord getLargestOfTwo (CSVRecord currentRow, CSVRecord largestSoFar) {
        //If largestSoFar is nothing
    if (largestSoFar == null) {
        largestSoFar = currentRow;
    }
    //Otherwise
    else {
        double currentTemp = Double.parseDouble(currentRow.get("TemperatureF"));
        double largestTemp = Double.parseDouble(largestSoFar.get("TemperatureF"));
        //Check if currentRow’s temperature > largestSoFar’s
        if (currentTemp > largestTemp) {
            //If so update largestSoFar to currentRow
        largestSoFar = currentRow;
            }
    }
    return largestSoFar;
   }
   
   public CSVRecord coldestHourInFile (CSVParser parser) {
       //start with largestSoFar as nothing
    CSVRecord smallestSoFar = null;
    //For each row (currentRow) in the CSV File
    for (CSVRecord currentRow : parser) {
    // use method to compare two records
        smallestSoFar = getSmallestOfTwo(currentRow, smallestSoFar);
    }
    //The largestSoFar is the answer
    return smallestSoFar;
   }
   
   public CSVRecord getSmallestOfTwo(CSVRecord currentRow, CSVRecord smallestSoFar) {
        
       //If largestSoFar is nothing
    if (smallestSoFar == null) {
        smallestSoFar = currentRow;
    }
    //Otherwise
    else {
        double currentTemp = Double.parseDouble(currentRow.get("TemperatureF"));
        double largestTemp = Double.parseDouble(smallestSoFar.get("TemperatureF"));
        //Check if currentRow’s temperature > largestSoFar’s
        if (currentTemp < largestTemp) {
            //If so update largestSoFar to currentRow
        smallestSoFar = currentRow;
            }
    }
    return smallestSoFar;
   }
   
   public String fileWithColdestTemperature () {
       String filename = "";
       File coldestSoFar = null;
       DirectoryResource directory = new DirectoryResource();
       for (File file:directory.selectedFiles()) {
           
           coldestSoFar = getColdestOfTwoFiles (file, coldestSoFar);
           
       }
       boolean coldestFound = coldestSoFar!= null;
       if (coldestFound) filename = coldestSoFar.getName(); 
       return filename;
   }
   
   public File getColdestOfTwoFiles (File currentFile, File coldestSoFarFile) {
       if (coldestSoFarFile == null) return currentFile;
       
       CSVParser currentFileData = (new FileResource(currentFile)).getCSVParser();
       CSVParser coldestSoFarData = (new FileResource(coldestSoFarFile)).getCSVParser();
       
       double currentColdestTemp = Double.parseDouble(
            (coldestHourInFile(currentFileData)).get("TemperatureF")
       );
       double coldestSoFarTemp = Double.parseDouble(
            (coldestHourInFile(coldestSoFarData)).get("TemperatureF")
       );
       
       if (currentColdestTemp < coldestSoFarTemp) return currentFile;
       else return coldestSoFarFile;
   }
   
   public void printCsvParser (CSVParser parser) {
       for (CSVRecord currentRow : parser) {
           System.out.printf(
                "%s: %s\n", 
                currentRow.get("DateUTC"), 
                currentRow.get("TemperatureF")
           );
       }
   }
   
   
   public CSVRecord lowestHumidityInFile (CSVParser parser) {
        CSVRecord lowestHumiditySoFar = null;
        //For each row (currentRow) in the CSV File
        for (CSVRecord currentRow : parser) {
            // use method to compare two records
            lowestHumiditySoFar = getLowestHumidityOfTwo(currentRow, lowestHumiditySoFar);
        }
        //The largestSoFar is the answer
        return lowestHumiditySoFar;
   }
   
   public CSVRecord getLowestHumidityOfTwo(CSVRecord currentRow, CSVRecord lowestHumiditySoFar) {
    boolean humidityHasValue = !(currentRow.get("Humidity").equals("N/A"));
       //If largestSoFar is nothing
    if (humidityHasValue && lowestHumiditySoFar == null) {
        lowestHumiditySoFar = currentRow;
    }
    //Otherwise
    if (humidityHasValue)  {
        double currentTemp = Double.parseDouble(currentRow.get("Humidity"));
        double largestTemp = Double.parseDouble(lowestHumiditySoFar.get("Humidity"));

        if (currentTemp < largestTemp)
            lowestHumiditySoFar = currentRow;
    }
    return lowestHumiditySoFar;
   }
   
   public CSVRecord lowestHumidityInManyFiles  () {
       CSVRecord lowestHumiditySoFar = null;
       DirectoryResource directory = new DirectoryResource();
       for (File file:directory.selectedFiles()) {
           FileResource fr = new FileResource(file);
           CSVRecord currentLowestHumidity = lowestHumidityInFile(fr.getCSVParser());
           
           lowestHumiditySoFar = getLowestHumidityOfTwo(currentLowestHumidity, lowestHumiditySoFar);
       }
        
       return lowestHumiditySoFar;
   }
   
   public double averageTemperatureInFile  (CSVParser parser) {
       double sumOfTemperatures = 0;
       int tempCount = 0;
       
       for (CSVRecord currentRow : parser) {
           tempCount++;
           sumOfTemperatures += Double.parseDouble(currentRow.get("TemperatureF"));
        }
        
       double average = sumOfTemperatures / tempCount;
       return average;
   }
   
   public double averageTemperatureWithHighHumidityInFile (CSVParser parser, int minHumidity) {
       double sumOfTemperatures = 0;
       int tempCount = 0;
       
       for (CSVRecord currentRow : parser) {
           boolean humidityHasValue = !(currentRow.get("Humidity").equals("N/A"));
           boolean isValidRow = humidityHasValue && Double.parseDouble(currentRow.get("Humidity")) >= minHumidity;
           
           if (isValidRow) {
               tempCount++;
               sumOfTemperatures += Double.parseDouble(currentRow.get("TemperatureF"));
           }        
        }
        
       double average = sumOfTemperatures > 0 ? sumOfTemperatures / tempCount: 0;
       return average;
   }
}
