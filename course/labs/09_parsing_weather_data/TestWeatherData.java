import edu.duke.*;
import org.apache.commons.csv.*;
import java.io.*;

public class TestWeatherData {
    WeatherData weather = new WeatherData();

    public void testHottestInDay () {
        FileResource fr = new FileResource("data/2015/weather-2015-01-02.csv");
    CSVRecord largest = weather.hottestHourInFile(fr.getCSVParser());
    System.out.println("hottest temperature was " + largest.get("TemperatureF") +
                   " at " + largest.get("TimeEST"));
    }
    
    public void testHottestInManyDays () {
    CSVRecord largest = weather.hottestInManyDays();
    System.out.println("hottest temperature was " + largest.get("TemperatureF") +
               " at " + largest.get("DateUTC"));
    }
    
    public void testColdestHourInFile () {
        FileResource fr = new FileResource("data/2015/weather-2015-01-02.csv");
    CSVRecord coldest = weather.coldestHourInFile(fr.getCSVParser());
    System.out.println("coldest temperature was " + coldest.get("TemperatureF") +
                   " at " + coldest.get("TimeEST"));
    }
    
    public void testFileWithColdestTemperature () {
        String file = weather.fileWithColdestTemperature();
        System.out.printf("Coldest day was in file %s \n", file);

        FileResource fr = new FileResource("data/2014/" + file);
        CSVRecord coldest = weather.coldestHourInFile(fr.getCSVParser());
        System.out.println("coldest temperature was " + coldest.get("TemperatureF"));
    System.out.println("All the Temperatures on the coldest day were:");
    weather.printCsvParser(fr.getCSVParser());
    }
    
    public void testLowestHumidityInFile() {
        FileResource fr = new FileResource("data/2014/weather-2014-01-20.csv");
	CSVRecord lowestHumidity = weather.lowestHumidityInFile(fr.getCSVParser());
	System.out.println("Lowest Humidity was " + lowestHumidity.get("Humidity") +
	           	   " at " + lowestHumidity.get("DateUTC"));
    }
    
    public void testLowestHumidityInManyFiles() {
        CSVRecord lowestHumidity = weather.lowestHumidityInManyFiles();
	System.out.println("Lowest Humidity was " + lowestHumidity.get("Humidity") +
	           	   " at " + lowestHumidity.get("DateUTC"));
    }
    
    public void testAverageTemperatureInFile() {
        FileResource fr = new FileResource("data/2014/weather-2014-01-20.csv");
	double averageTemperature = weather.averageTemperatureInFile(fr.getCSVParser());
	System.out.println("Average Temperature in file is " + averageTemperature);
    }
    
    public void testAverageTemperatureWithHighHumidityInFile() {
//         FileResource fr = new FileResource("data/2014/weather-2014-01-20.csv");
        FileResource fr = new FileResource("data/2014/weather-2014-03-20.csv");
	double averageTemperature = weather.averageTemperatureWithHighHumidityInFile(fr.getCSVParser(), 80);
	
	if (averageTemperature == 0) {
	    System.out.println("No temperatures with that humidity");
	} else {
	    System.out.println("Average Temp with high Humidity is " + averageTemperature);
	}
    }
}
