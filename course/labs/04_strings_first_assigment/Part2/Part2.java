package Part2;

public class Part2 {
  final String START_CODON = "ATG";
  final String STOP_CODON = "TAA";

  public static void main (String[] args) {
    Part2 geneFinder = new Part2();
    geneFinder.testSimpleGene();
  }

  public void testSimpleGene () {
    String dnaWithoutATG = "CCCATTACCTAA"; // result ""
    String dnaWithoutTAA = "CCCATGTACCTAA"; // result ""
    String dnaWithInvalidLength = "CCCATGCCCPYOCTAACCC"; // result ""
    String validDna = "CCCATGCCCpYUIOCTAACCC"; // result "ATGCCCPYUIOCTAA"

    String param = validDna; // replace by testing value
    System.out.printf("Given param: %s \n",  param);
    
    String dna = this.findSimpleGene(param, this.START_CODON, this.STOP_CODON);
    System.out.printf("Result: %s \n", dna);
  }

  public String findSimpleGene (String dna, String startCodon, String stopCodon) {
    dna = dna.toUpperCase();
    int startCodonIndex = dna.indexOf(startCodon);
    boolean startCodonNotFound  = startCodonIndex == -1;
    if (startCodonNotFound) {
      System.out.printf("ERROR: Not found start codon (ATG) in %s \n", dna);
      return "";
    }


    int stopCodonIndex = dna.indexOf(stopCodon, startCodonIndex + startCodon.length());
    boolean stopCodonNotFound = stopCodonIndex == -1;
    if (stopCodonNotFound) {
      System.out.printf("ERROR: Not found stop codon (TAA) in: %s \n", dna);
      return "";
    }

    String codon = dna.substring(startCodonIndex, stopCodonIndex + stopCodon.length());
    int codonSize = codon.length();
    boolean codonInvalid = codonSize % 3 != 0;
    if (codonInvalid) {
      System.out.printf(
        "ERROR: codon (%s) must be a multiple of 3, but it only have %s characters  \n", 
        codon, 
        codonSize
      );
      return "";
    }

    return codon;
  }


}