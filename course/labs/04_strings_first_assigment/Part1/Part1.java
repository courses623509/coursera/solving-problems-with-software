public class Part1 {
  final String START_CODON = "ATG";
  final String STOP_CODON = "TAA";

  public static void main (String[] args) {
    Part1 stringAssignmentPart1 = new Part1();
    stringAssignmentPart1.testSimpleGene();
  }

  public void testSimpleGene () {
    String dnaWithoutATG = "CCCATTACCTAA"; // result ""
    String dnaWithoutTAA = "CCCATGTACCTAA"; // result ""
    String dnaWithInvalidLength = "CCCATGCCCPYOCTAACCC"; // result ""
    String validDna = "CCCATGCCCPYUIOCTAACCC"; // result "ATGCCCPYUIOCTAA"

    String param = dnaWithInvalidLength; // replace by testing value
    System.out.printf("Given param: %s \n",  param);
    
    String dna = this.findSimpleGene(param);
    System.out.printf("Result: %s \n", dna);
  }

  public String findSimpleGene (String dna) {
    int startCodonIndex = dna.indexOf(this.START_CODON);
    boolean startCodonNotFound  = startCodonIndex == -1;
    if (startCodonNotFound) {
      System.out.printf("ERROR: Not found start codon (ATG) in %s \n", dna);
      return "";
    }


    int stopCodonIndex = dna.indexOf(this.STOP_CODON, startCodonIndex + this.START_CODON.length());
    boolean stopCodonNotFound = stopCodonIndex == -1;
    if (stopCodonNotFound) {
      System.out.printf("ERROR: Not found stop codon (TAA) in: %s \n", dna);
      return "";
    }

    String codon = dna.substring(startCodonIndex, stopCodonIndex + this.STOP_CODON.length());
    int codonSize = codon.length();
    boolean codonInvalid = codonSize % 3 != 0;
    if (codonInvalid) {
      System.out.printf(
        "ERROR: codon (%s) must be a multiple of 3, but it only have %s characters  \n", 
        codon, 
        codonSize
      );
      return "";
    }

    return codon;
  }


}