 
class Part3 {
  // {{textToSearch, sampleText}, ...}
  final String[][] MOCK_TWO_OCCURRENCES = {
    {"na", "banana"},
    {"ka", "banana"}
  };

  // {{textToSearch, sampleText}, ...}
  final String[][] MOCK_LAST_PART = {
    {"na", "banana"},
    {"nothing", "banana"}
  };

  public static void main (String[] args) {
    Part3 tester = new Part3();
    tester.testTwoOccurrences();
    tester.testLastPart();
  }

  public void testTwoOccurrences() {
    for (String[] params: this.MOCK_TWO_OCCURRENCES ) {
      boolean hasTwoOccurrences = this.twoOccurrences(
        params[0],
        params[1]
      );

      String resultText = hasTwoOccurrences ? "has": "doesn't have";

      System.out.printf(
        "Given param '%s' %s two or more occurrences in '%s' \n",
        params[0],
        resultText,
        params[1]
      );
    }
  }

  public void testLastPart () {
    for (String[] params: this.MOCK_LAST_PART ) {
      String lastPart = this.lastPart(
        params[0],
        params[1]
      );

      System.out.printf(
        "The part of the string after '%s' in '%s' is '%s' \n",
        params[0],
        params[1],
        lastPart
      );
    }
  }

  public boolean twoOccurrences(String find, String text) {
    int occurrenceCount = 0;
    int startIndexOf = 0;

    do {
      int occurrenceIndex = text.indexOf(find, startIndexOf);
      boolean occurrenceFound = occurrenceIndex != -1;

      if (occurrenceFound) {
        occurrenceCount++;
        startIndexOf++;
      } else {
        startIndexOf = -1;
      }
    } while(startIndexOf < text.length() && startIndexOf != -1) ;

    boolean hasTwoOccurrences = occurrenceCount >= 2;
    return hasTwoOccurrences;
  }

  public String lastPart (String find, String text) {
    int occurrenceIndex = text.indexOf(find);
    boolean occurrenceFound = occurrenceIndex != -1;

    String result = text;

    if (occurrenceFound) {
      result = text.substring(occurrenceIndex, text.length());
    }
    return result;
  }
}