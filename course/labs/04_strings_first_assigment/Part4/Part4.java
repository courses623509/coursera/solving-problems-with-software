
/**
 * Write a description of class Part4 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

import edu.duke.*;

public class Part4 {
    final String ORIGIN_FILE = "./manylinks.html";

    public static void main (String[] args) {
      Part4 test = new Part4();
      test.testFindWebLinks();
    }

    public void testFindWebLinks() {
      this.findWebLinks(this.ORIGIN_FILE);
    }

    public String[] findWebLinks(String weblink) {
      String[] result = {};
      FileResource pageContent = new FileResource(weblink);
      for (String word: pageContent.words()) {
        boolean containsYoutube = word.indexOf("youtube.com") != -1;
          if (containsYoutube) {
              int startUrlIndex = word.indexOf("\"");
              boolean startUrlNotFound = startUrlIndex == -1;
              if (startUrlNotFound) {
                  continue;
              }
              
              int stopUrlIndex = word.indexOf("\"", startUrlIndex + 1);
              boolean stopUrlNotFound = stopUrlIndex == -1;
              if (stopUrlNotFound) {
                  continue;
              }
              
              String url = word.substring(startUrlIndex, stopUrlIndex + 1); 
              System.out.printf("Found: %s\n", url);
          } 
      }
      return result;
    }
}
