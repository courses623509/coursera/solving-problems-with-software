import org.apache.commons.csv.*;
import edu.duke.*;

public class TestCountryExports {
    final String[][] MOCK_COUNTRY_INFO = {
        {"Germany"}
    };
    CountryExports countryExports = new CountryExports();
    
    public void testCountryInfo () {
        FileResource fr = new FileResource();
        CSVParser parser = fr.getCSVParser();
        String countryInfo = countryExports.countryInfo(parser, "Nauru");
        System.out.println(countryInfo);
    }
    
    public void testListExportersTwoProducts () {
        FileResource fr = new FileResource();
        CSVParser parser = fr.getCSVParser();
        countryExports.listExportersTwoProducts (parser, "gold", "diamonds");
    }
    
    public void testNumberOfExporters () {
        FileResource fr = new FileResource();
        CSVParser parser = fr.getCSVParser();
        int exporters = countryExports.numberOfExporters (parser, "sugar");
        System.out.println(exporters);
    }
    
    public void testBigExporters () {
        FileResource fr = new FileResource();
        CSVParser parser = fr.getCSVParser();
        countryExports.bigExporters (parser, "$999,999,999,999");
    }
}
