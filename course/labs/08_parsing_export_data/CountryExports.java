
import org.apache.commons.csv.*;

public class CountryExports {
  public String countryInfo (CSVParser parser, String searchedCountry) {
    String countryInfo = "NOT FOUND";
    for (CSVRecord record: parser) {
      String currentCountry = record.get("Country");
      boolean countryFound = currentCountry.equals(searchedCountry);
      
      if (countryFound) {
        String exports = record.get("Exports");
        String amount = record.get("Value (dollars)");
        countryInfo = currentCountry + ": " + exports + ": " + amount;
      }
    }
    return countryInfo;
  }
    
  public void listExportersTwoProducts (CSVParser parser, String exportItem1, String exportItem2) {
      for (CSVRecord record: parser) {
        String exports = record.get("Exports");
        boolean exportItem1Found = exports.contains(exportItem1);
        boolean exportItem2Found = exports.contains(exportItem2);
        boolean twoItemsFound = exportItem1Found && exportItem2Found;
        
        if (twoItemsFound) {
            String country = record.get("Country");
            System.out.println(country);
        }
      }
  }
  
  public int numberOfExporters (CSVParser parser, String searchedItem) {
      int exportersCount = 0;
      for (CSVRecord record: parser) {
          String exports = record.get("Exports");
          boolean searchedItemExported = exports.contains(searchedItem);
          
          if (searchedItemExported) {
            exportersCount++;
          }
          
      }
      return exportersCount;
  }
  
  public void bigExporters (CSVParser parser, String amount) {
    for (CSVRecord record: parser) {
        String currentValue = record.get("Value (dollars)");
        boolean isCurrentValueBiggerThanAmount = currentValue.length() > amount.length();
        
        if (isCurrentValueBiggerThanAmount) {
            String country = record.get("Country");
            System.out.println(country + " " + currentValue);
        }
    }
  }
}
