package Part02;

public class TestPart2 {
    Part2 part2 = new Part2();
  
      final String[][] MOCK_CG_RATIO = {
        {"ATGAACGAATTGATC", "5/15 => 0.3333"},
        {"ATAAAA", "0/6 => 0"},
        {"ATGCCATAG", "4/9 => 0.44444"}
      };
  
  public void testCgRatio () {
    for (String[] testCase: this.MOCK_CG_RATIO) {
        String dna = testCase[0];
        String expectedResult = testCase[1];
          
        float result = part2.cgRatio(dna);
        
        System.out.printf(
          "When cgRatio(dna: '%s') is called \n", 
          dna
        );
        System.out.printf(
          "Expected Result: %s    Real result: %f\n\n",
          expectedResult, result
        );   
      }
  }
}
