package Part02;


/**
 * Write a description of Part2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Part2 {
  public float cgRatio (String dna) {
      int cOccurrences = this.howMany("C", dna);
      int gOccurrences = this.howMany("G", dna);
      
      float ratio = (float) (cOccurrences + gOccurrences) / dna.length();
      return ratio;
  }  
   
  public int howMany (String stringA, String stringB) {
    // start counter of occurrences in 0
    int occurrences = 0;

    // start start index to search in 0
    int searchStartIndex = 0;
    int occurrenceIndex = stringB.indexOf(stringA);
    
    while (occurrenceIndex != -1) {
      occurrences++;
      searchStartIndex = occurrenceIndex + stringA.length();
      occurrenceIndex = stringB.indexOf(stringA, searchStartIndex);
    }
    
    //System.out.printf("Occurrences of '%s' found in '%s' %d \n", stringA, stringB, occurrences);
    return occurrences;
  }
}
