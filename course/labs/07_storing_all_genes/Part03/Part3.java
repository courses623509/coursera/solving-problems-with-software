package Part03;

import edu.duke.*;
import Part01.*;
import Part02.*;

public class Part3 {
    Part1 part1 = new Part1();
    Part2 part2 = new Part2();
    
    public void processGenes (StorageResource geneList) {
        int genesLongerThanNineChars = 0;
        int genesWithRatioGreaterThanDecimal0_35 = 0;
        int longestGeneSizeFound = 0;
        int totalGenes = 0;
        int counterOfCTG = 0;
        
        for (String gene: geneList.data()) {
            totalGenes++;
            int geneSize = gene.length();
            longestGeneSizeFound = Math.max(longestGeneSizeFound, geneSize);
            counterOfCTG += part2.howMany("CTG", gene);
            if (geneSize > 60) {
                System.out.printf("%s is longer than 60 \n", gene);
                genesLongerThanNineChars++;
            }

            if (part2.cgRatio(gene) > 0.35) {
                System.out.printf("%s has ration greater than 0.35\n", gene);
                genesWithRatioGreaterThanDecimal0_35++;
            }            
        }
        System.out.printf(
            "Number of genes where it was longer than 60: %d \n", 
            genesLongerThanNineChars
        );
        System.out.printf(
            "Number of genes where its genes ratio was greater than 0.35: %d \n", 
            genesWithRatioGreaterThanDecimal0_35
        );
        System.out.printf(
            "The longest gene found was %d characters \n\n", 
            longestGeneSizeFound
        );
        System.out.printf(
            "Total Genes %d \n\n", 
            totalGenes
        );
        System.out.printf("CTG was found %d in DNA", counterOfCTG);
    }
    
    public String mystery(String dna) {
      int pos = dna.indexOf("T");
      int count = 0;
      int startPos = 0;
      String newDna = "";
      if (pos == -1) {
        return dna;
      }
      while (count < 3) {
        count += 1;
        newDna = newDna + dna.substring(startPos,pos);
        startPos = pos+1;
        pos = dna.indexOf("T", startPos);
        if (pos == -1) {
          break;
        }
      }
      newDna = newDna + dna.substring(startPos);
      return newDna;
    }
}
