package Part03;

import edu.duke.*;
import Part01.*;
import Part02.*;

public class TestPart3 {
  final String[][] MOCK_GET_ALL_GENE = {
      { // gene longer than 9
          "xxxxxxxxxxxxxATGxxxxxxTAAxxxxATGxxxTAGxxxTAAxxxTGAxxxATGxTAAxxTAAxxATGxxxx" 
       },
       {
           "ATGTAA"
        },
       { "AATGCCCACTAGCTGACTAAT", "ATGCTAACTAGCTGA"}
  };  
  
  Part1 part1 = new Part1();
  Part2 part2 = new Part2();
  Part3 part3 = new Part3();
  
  public void testProcessGenes () {
      FileResource fr = new FileResource("./Part03/brca1line.txt");
      String dna = fr.asString();
          
      StorageResource genes = part1.getAllGenes(dna);
      part3.processGenes(genes);
      
      
  }
  
  public void testMystery () {
    String mysteryResult = part3.mystery("AATGCCCACTAGCTGACTAAT");
    System.out.println(mysteryResult);
  }
}
