package Part01;

import edu.duke.*;



public class TestPart1 {
  Part1 part1 = new Part1(); 
    // {{dna, expected}}
  final String[][] MOCK_GET_ALL_GENE = {
      {
          "xxxxxxxxxxxxxATGxxxxxxTAAxxxxATGxxxTAGxxxTAAxxxTGAxxxATGxTAAxxTAAxxATGxxxx", 
          "ATGxxxxxxTAA\nATGxxxTAG\nATGxTAAxxTAA"
       },
       { "AATGCTAACTAGCTGACTAAT", "ATGCTAACTAGCTGA"}
  };
  
  public void testGetAllGenes () {
      for (String[] testCase: this.MOCK_GET_ALL_GENE) {
          String dna = testCase[0];
          String expectedResult = testCase[1];
          
          System.out.printf("When printAllGenes(dna: '%s') is called \n", dna);
          System.out.printf("Expected Result:\n%s \n\n   Real result: \n", expectedResult);   
          
          StorageResource genes = part1.getAllGenes(dna);
          for (String gene: genes.data()) {
            System.out.println(gene);
          }
          System.out.printf("\n\n\n");
        
      }
  }
  
}
