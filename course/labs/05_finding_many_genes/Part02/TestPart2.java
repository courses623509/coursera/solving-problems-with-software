package Part02;


/**
 * Write a description of TestPart2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class TestPart2 {
  Part2 part2 = new Part2();
  
  final String[][] MOCK_HOW_MANY = {
    {"GAA", "ATGAACGAATTGAATC", "3"},
    {"AA", "ATAAAA", "2"}
  };
  
  public void howMany () {
    for (String[] testCase: this.MOCK_HOW_MANY) {
        String searchedText = testCase[0];
        String testString = testCase[1];
        String expectedResult = testCase[2];
          
        int result = part2.howMany(searchedText, testString);
        String resultString = result == Integer.parseInt(expectedResult) ? "SUCCESS": "ERROR";
        
        System.out.printf(
          "When howMany(searchedText: '%s', testString: '%s') is called \n", 
          searchedText, testString
        );
        System.out.printf(
          "%s=> Expected Result: %s    Real result: %d\n\n",
          resultString, expectedResult, result
        );   
      }
  }
}
