package Part02;


/**
 * Write a description of Part2 here.
 * 
 * @author (Erick Saravia) 
 * @version (a version number or a date)
 */
public class Part2 {
  public int howMany (String stringA, String stringB) {
    // start counter of occurrences in 0
    int occurrences = 0;

    // start start index to search in 0
    int searchStartIndex = 0;
    int occurrenceIndex = stringB.indexOf(stringA);
    
    while (occurrenceIndex != -1) {
      occurrences++;
      searchStartIndex += stringA.length() + 1;
      occurrenceIndex = stringB.indexOf(stringA, searchStartIndex);
    }

    return occurrences;
  }
}
