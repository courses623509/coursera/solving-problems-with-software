package Part03;


/**
 * Write a description of TestPart3 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class TestPart3 {
  final String[][] MOCK_COUNT_GENES = {
      {"xxxxxxxxxxxxxATGxxxxxxTAAxxxxATGxxxTAGxxxTAAxxxTGAxxxATGxTAAxxTAAxxATGxxxx","3"},
      {"ATGxxxxxxTAAxxxxATGxxxTAG", "2"},
      {"xxxxxxx", "0"},
      {"ATGxxxxxx", "0"}
  };
  Part3 part3 = new Part3(); 
  
  public void testCountGenes () {
      for (String[] testCase: this.MOCK_COUNT_GENES) {
          String dna = testCase[0];
          String expectedResult = testCase[1];
          
          int result = part3.countGenes(dna);
          
          String resultString = result == Integer.parseInt(expectedResult) ? "SUCCESS": "ERROR";
          System.out.printf("When CountGenes(dna: '%s') is called \n", dna);
          System.out.printf(
            "%s=>Expected Result: %s    Real result: %d \n\n", 
            resultString, expectedResult, result
          ); 
      }  
  }
}
