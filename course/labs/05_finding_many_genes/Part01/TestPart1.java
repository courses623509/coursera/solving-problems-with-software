package Part01;

import Part01.*;

/**
 * Write a description of class TestPart1 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class TestPart1 {
  Part1 part1 = new Part1(); 
    // {{gived, searchTag, expected}}
  final String[][] MOCK_FIND_STOP_CODON = {
    {"xxxATGxxxxxxTAA", "3", "TAA",  "12"},
    {"xxxATGxxxxxxTAA", "3", "TAG",  "-1"},
    {"xxxATGxxxxxTAG", "3", "TAG",  "-1"},
    {"xxxATGxTAAxxTAA", "3", "TAA",  "12"},
  };
  
  final String[][] MOCK_FIND_GENE = {
      {"xxxxxxxxxx", ""}, // 'ATG' not found
      {"xxxATGxxxxxxTAA", "ATGxxxxxxTAA"},  // One valid stop Codon
      {"xxxxATGxxxTAGxxxTAAxxxTGA", "ATGxxxAAG"}, // three valid stop codons
      {"xxxATGxTAAxxTAA", "ATGxTAAxxTAA"}, // 'ATG' invalid stop codon before valid stop codon
      {"xxATGxxxx", ""} // 'ATG' without valid stop codon
      
  };
  
  final String[][] MOCK_PRINT_ALL_GENE = {
      {
          "xxxxxxxxxxxxxATGxxxxxxTAAxxxxATGxxxTAGxxxTAAxxxTGAxxxATGxTAAxxTAAxxATGxxxx", 
          "ATGxxxxxxTAA\nATGxxxTAG\nATGxTAAxxTAA"
       },
       { "AATGCTAACTAGCTGACTAAT", "ATGCTAACTAGCTGA"}
  };
  
  public void testPrintAllGenes () {
      for (String[] testCase: this.MOCK_PRINT_ALL_GENE) {
          String dna = testCase[0];
          String expectedResult = testCase[1];
          
          System.out.printf("When printAllGenes(dna: '%s') is called \n", dna);
          System.out.printf("Expected Result:\n %s \n\n   Real result: \n", expectedResult);   
          
          part1.printAllGenes(dna);
          
          System.out.printf("\n\n\n");
        
      }
  }
  
  public void testFindGene () {
      for (String[] testCase: this.MOCK_FIND_GENE) {
          String dna = testCase[0];
          String expectedResult = testCase[1];
          
          String result = part1.findGene(dna);
          
        System.out.printf("When findGene(dna: '%s') is called \n", dna);
        System.out.printf("Expected Result: %s    Real result: %s\n\n", expectedResult, result);   
      }
  }
  
  public void testfindStopCodon() {
    for (String[] testCase: this.MOCK_FIND_STOP_CODON ) {
        String dna = testCase[0];
        int startIndex = Integer.parseInt(testCase[1]);
        String stopCodon = testCase[2];
        String expectedResult = testCase[3];
        
        int result = part1.findStopCodon(dna, startIndex, stopCodon);
        System.out.printf(
            "When findStopCodon(dna: '%s', startIndex: %d, stopCodon: '%s') is called \n",
            dna, startIndex, stopCodon
        );
        System.out.printf("Expected Result: %s    Real result: %d\n\n", expectedResult, result);   
    }
  }
}
