package Part01;

class Part1 {
  final String START_CODON = "ATG";
  final String[] STOP_CODONS = { "TAA", "TAG", "TGA" };
  
  public void printAllGenes (String dna) {
      boolean searchCompleted = false;
      do {
        String gene = this.findGene(dna);
        boolean geneNotFound = gene.equals("");
        if (geneNotFound) {
            break;
        }
        
        System.out.println(gene);
        
        int startGeneIndex = dna.indexOf(gene);
        int startNewDNAIndex = startGeneIndex + gene.length();
        System.out.printf(
            "Current gene: %s \nSearching gene using new dna starting on %d : %s", 
            gene, startNewDNAIndex, dna
        );
        dna = dna.substring(startNewDNAIndex);
        
      } while (true);
  }
  
  public String findGene (String dna) {
    int startIndex = dna.indexOf(this.START_CODON);
    boolean startIndexNotFound = startIndex == -1;
    if (startIndexNotFound) return "";

    int stopIndex = dna.length();
    for (String stopCodon: this.STOP_CODONS) {
      int tempStopIndex = this.findStopCodon(dna, startIndex, stopCodon);
      boolean tempStopIndexInvalid = tempStopIndex == -1;

      if (tempStopIndexInvalid) {
        continue;
      } else {
        stopIndex = Math.min(stopIndex, tempStopIndex);
      }
    }

    boolean stopIndexUnchanged = stopIndex == dna.length();
    if (stopIndexUnchanged) return "";
    return dna.substring(startIndex, stopIndex + 3);
  }

  public int findStopCodon (String dna, int startIndex, String stopCodon) {
    int currentIndex = startIndex + 3;
    int stopIndex = dna.indexOf(stopCodon, currentIndex);
    while (stopIndex != -1) {
        int diff = stopIndex - startIndex;
        boolean stopIndexIsInvalid = diff % 3 != 0;
        if (stopIndexIsInvalid) {
          stopIndex = dna.indexOf(stopCodon, stopIndex + 1);
        } else {
          return stopIndex;
        }
      }
      return stopIndex;
    } 
}