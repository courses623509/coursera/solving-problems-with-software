package practices;

import java.util.Scanner;

public class _03_BasicInputOutput {
  public static void main (String[] args ) {
    _03_BasicInputOutput.outputs();
    _03_BasicInputOutput.inputs();
  }

  public static void outputs () {
    System.out.print("The next text will be printed after this.");
    System.out.println("Move cursor after this text to next line");
    
    System.out.printf("Scape a decimal format: %d \n", 6);
    System.out.printf("Scape a String format: %s \n", "Hello");
    System.out.printf("Scape a float number with 3 decimals: %.3f \n", 9.653148);
    System.out.printf("Exponent notation of floating-point number: %f \n", 8.1256464688);
    // Learn about Format Specifiers: https://www.programiz.com/cpp-programming/library-function/cstdio/printf
  }

  public static void inputs () {
    int age;
    String name;
    double shirtPrice;
    Scanner input = new Scanner(System.in);

    System.out.print("Hello, What's your name?: ");
    name = input.next();
    
    System.out.print("How old are you?: ");
    age = input.nextInt();
    
    System.out.print("You're wearing a nice shirt, how much it?: ");
    shirtPrice = input.nextDouble();
    
    System.out.printf(
      "Hello %s, you tell me that your age is %d, and your shirt is $%e", name, age, shirtPrice);
    input.close();
  }
}
