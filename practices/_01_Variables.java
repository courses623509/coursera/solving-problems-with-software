package practices;

public class _01_Variables {
  
  // Identifiers
  public static int age = 24; // simple
  public static int _age = 24; // using underscore before
  public static int $age = 24; // using dollar sign before

  //------- DATA TYPES
  // Java literals
  public static int yearsOfExperience = 3;
  public static char firstNameChar = 'E'; 
  public static float weightInPounds = 128;

  // 1. Boolean literals
  public static boolean singer = true;

  // 2. Integer Literals
  public static int twentyAsBinaryNumber = 0b10100; // Binaries starts with 0b
  public static int twentyAsOctalNumber = 024; // Octal starts with 0
  public static int twentyAsDecimalNumber = 20; // Decimal doesn't have any changes
  public static int twentyAsHexadecimalNumber = 0x14; // Hexadecimals starts with 0x

  //3. Floating-point Literals
  public static double coinsInHerWallet = 10.5;
  public static float coinsInHisWallet = 10.5F;

  // 4. Character Literals
  // Use single quotes to enclose an UNICODE character
  // Use escape sequences as character literals. For example, 
  // \b (backspace), \t (tab), \n (new line)
  public static char firstLetterOfTheAlphabet = 'a'; 

  // 5. String Literals
  public static String firstName = "Erick";
  public static String lastName = "Saravia";
  public static String fullName = _01_Variables.firstName + ' ' + _01_Variables.lastName;

  public static void main (String[] args ) {
    _01_Variables.printGreeting(_01_Variables.fullName);
    // Replace the variable name after "_01_Variables" to print it.
    // For Example: _01_Variables.myPublicStaticProperty
    System.out.println(_01_Variables.twentyAsHexadecimalNumber);
  }

  
  public static void printGreeting (String personName) {
    String greeting = "Hello, " + personName + "\n";;
    greeting += "Let's go to practice!!!";
    System.out.println(greeting);
  }
}
