package practices;

import java.util.Scanner;

public class _04_Loops {
  public static Scanner inputScanner;
  public static boolean isInputScannerOpen = false;
  public static int[] numbers = {};

  public static void main (String[] args ) {
    int firstNumberOfTheList = _04_Loops.getInt("Enter the first number of the list: ");
    int lastNumberOfTheList = _04_Loops.getInt("Enter the last number of the list: ");
    
    _04_Loops.setNumbers(firstNumberOfTheList, lastNumberOfTheList);
    _04_Loops.printNumbers();

    _04_Loops.toggleScanner(false);
  }

  public static int getInt (String message) {
    Scanner input = _04_Loops.getScanner();
    System.out.print(message);
    int userInput = input.nextInt();
    return userInput;
  }

  public static void setNumbers(
    int firstNumber,
    int lastNumber
  ) {
    int arrayLength = lastNumber - firstNumber + 1;
    _04_Loops.numbers = new int[arrayLength];

    for (int value = firstNumber, i = 0; value <= lastNumber; i++, value++) {
      _04_Loops.numbers[i] = value;  
    }
  }

  public static void printNumbers () {
    System.out.println("List values: ");
    for (int number:_04_Loops.numbers) {
      System.out.println(number);
    }
  }

  public static Scanner getScanner () {
    if (!_04_Loops.isInputScannerOpen) {
      _04_Loops.toggleScanner(true);
    }
    return _04_Loops.inputScanner;
  }

  public static void toggleScanner (boolean shouldItBeEnabled) {
    if (shouldItBeEnabled) {
      _04_Loops.inputScanner = new Scanner(System.in);
      _04_Loops.isInputScannerOpen = true;
    } else {
      _04_Loops.inputScanner.close();;
      _04_Loops.isInputScannerOpen = false;
    }
  }

}
