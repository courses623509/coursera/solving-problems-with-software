package practices;

public class _02_DataTypes {
  //---- PRIMITIVE DATA TYPES

  //--- 1. Boolean type
  public static boolean isThisCodeTheCleanestInTheWorld = false;
  public static boolean isThisCodeWroteInJava = true;

  /* --- 2. Byte type 
   * It can have values from -128 to 127 (8-bit bit signed)
   * Use to save memory
  */
  public static byte age = 24;
  public static byte fingersInMyHand = 5;
  // public static byte numberOutOfRange = -129; // Uncomment to see the error alert

  /* --- 3. Short type
   *It can have values from -32768 to 32767 (16-bit signed two's complement integer).
   */
  public static short temperature = -200;

  /* --- 4. Int type
   * It can have values from -2^31 to (2^31)-1 (32-bit bit signed)
   */
  public static int kilobytesIntoAMegabytes = 1024;

  /* --- 5. Long type
   * It can have values from -2^63 to (2^63)-1(64-bit signed two's complement integer)
   * Add 'L' at the end to represent that it's an integer of the long type
   */
  public static long longNumber = -42332200000L; 

  /* --- 6. Double Type */
  public static double decimalWith64BitPrecision = -45.6;

  /* --- 7. Single Precision
   * It is a single-precision 32-bit floating-point
   * Never used for precise values as currency
   */
  public static float singlePrecision = -42.3f;

  /* --- 8 Char type
   * It's a 16-bit Unicode Character
   * The minimum value of the char data type is '\u0000' (0) and the maximum value of the is '\uffff'.
   */
  public static char letterQAsHexadecimalUniCode = '\u0051';
  public static char letterQAsDecimalUnicode = 81;
  public static char letterQ = 'Q'; 

  public static void main (String[] args ) {
    System.out.println(_02_DataTypes.letterQAsHexadecimalUniCode);
   }
}
